from django.db import models
from django.core.validators import MinLengthValidator


class Category(models.Model):
    name = models.CharField('Category name', max_length=16, validators=[MinLengthValidator(4)])
    order_number = models.PositiveSmallIntegerField('Order number', default=0)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categorys'

    def __str__(self):
        return self.name
