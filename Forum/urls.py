from django.urls import path
from django.views.generic import RedirectView
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.index_view, name='index'),

]
