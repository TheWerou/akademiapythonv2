from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, ListView

from .models import Category


class IndexView(ListView):
    template_name = "index.html"
    model = Category


index_view = IndexView.as_view()
